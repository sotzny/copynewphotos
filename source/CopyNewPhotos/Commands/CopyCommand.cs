﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ManyConsole;

namespace CopyNewPhotos.Commands
{
    public class CopyCommand : ConsoleCommand
    {
        private const int Success = 0;
        private const int Failure = 2;

        public int MaxDays { get; set; }
        public int MaxMegabytes { get; set; }

        public int MaxPhotos { get; set; }
        public bool Recursiv { get; set; }

        public string SourceFolder { get; set; }
        public string TargetFolder { get; set; }

        public string PhotoFileTypes { get; set; }

        public CopyCommand()
        {
            IsCommand("copy", "Copy new Photos with Parameters");

            HasOption("d|days=", "maximum age of the photos (Default 30)", p => MaxDays = int.Parse(p));
            HasOption("s|size=", "Maximum data volume (Default 1024)", p => MaxMegabytes = int.Parse(p));
            HasOption("c|count=", "maximum number of Photos (Default 1000)", p => MaxPhotos = int.Parse(p));
            HasOption("r|recursiv", "the search should be recursive", p => Recursiv = p != null);
            HasOption("photoFileTypes", "File extensions of photos (Default:  jpg,jpeg", p => PhotoFileTypes = p);
            HasRequiredOption("i|input=", "input Folder", p => SourceFolder = p);
            HasRequiredOption("o|output=", "output Folder", p => TargetFolder = p);

            SetDefaults();
        }

        public override int Run(string[] remainingArguments)
        {
            //  SetDefaults();
            var filetypes = PhotoFileTypes.Split(',', ';');

            var files = new List<string>();
            foreach (var filetype in filetypes)
            {
                files.AddRange(Directory.GetFiles(SourceFolder, $"*.{filetype}",
                    Recursiv ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
            }

            var fileinfoList = files.Select(p => new FileInfo(p)).OrderByDescending(p => p.CreationTime).Take(MaxPhotos).ToList();

            //max Days
            fileinfoList = fileinfoList.Where(p => (DateTime.Now - p.CreationTime).TotalDays < MaxDays).ToList();

            // Max Bytes
            var sumBytes = 0L;
            var stopAt = MaxMegabytes * 1024 * 1024;
            var resultList = new List<FileInfo>();
            foreach (var fi in fileinfoList)
            {
                if ((sumBytes + fi.Length) > stopAt) break;

                sumBytes += fi.Length;
                resultList.Add(fi);
            }

            Console.WriteLine($"Found {resultList.Count} Files with your filter criteria.");

            var cntDelete = 0;
            var cntCopy = 0;
            // Delete old
            var targetFiles = Directory.GetFiles(TargetFolder, "*.*").Select(p => new FileInfo(p)).ToList();
            foreach (var fi in targetFiles)
            {
                if (resultList.Any(p => p.Name == fi.Name)) continue;
                File.Delete(fi.FullName);
                Console.WriteLine($"Delete old File {fi.Name}");
                cntDelete++;
            }

            foreach (var fi in resultList)
            {
                if (File.Exists(Path.Combine(TargetFolder, fi.Name))) continue;
                File.Copy(fi.FullName, Path.Combine(TargetFolder, fi.Name),true);
                Console.WriteLine($"Copy {fi.Name}");
                cntCopy++;
            }

            Console.WriteLine($"Copied {cntCopy} Files and deleted {cntDelete} old Files");

            return 0;
        }

        private void SetDefaults()
        {
            if (MaxDays < 1) MaxDays = 30;
            if (MaxMegabytes < 1) MaxMegabytes = 1024;
            if (MaxPhotos < 1) MaxPhotos = 1000;
            if (string.IsNullOrWhiteSpace(PhotoFileTypes)) PhotoFileTypes = "jpg,jpeg";
        }
    }
}