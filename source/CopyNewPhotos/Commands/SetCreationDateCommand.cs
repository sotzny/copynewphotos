﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using ManyConsole;
using MetadataExtractor;
using Directory = System.IO.Directory;

namespace CopyNewPhotos.Commands
{
    public class SetCreationDateCommand : ConsoleCommand
    {
        public SetCreationDateCommand()
        {
            IsCommand("ResetCreationTime", "Sets the file creation date to the date the photo was created in all SubDirectories");
            HasRequiredOption("i|input=", "input Folder", p => SourceFolder = p);
        }

        public string SourceFolder { get; set; }

        public override int Run(string[] remainingArguments)
        {
            var filetypes = "*.jpg,*.jpeg".Split(',');
            CultureInfo provider = CultureInfo.InvariantCulture;


            foreach (var filetype in filetypes)
            {
                var files = Directory.GetFiles(SourceFolder, filetype, SearchOption.AllDirectories);
                Console.WriteLine($"Found {files.Length} Files in {SourceFolder}");
                foreach (var file in files)
                {
                    var fi = new FileInfo(file);
                    Console.Write($"Process File: {fi.Name} ");
                    IReadOnlyList<MetadataExtractor.Directory> directories = ImageMetadataReader.ReadMetadata(file);

                    MetadataExtractor.Directory exif = directories.FirstOrDefault(p => p.Name == "Exif SubIFD");
                    if (exif == null)
                    {
                        Console.WriteLine($"At {file} we found no Exif Data");
                        continue;
                    }

                    var datum = exif.Tags.FirstOrDefault(p => p.Name == "Date/Time Original");
                    if (datum == null)
                    {
                        Console.WriteLine($"At {file} we found no creation Date");
                        continue;
                    }

                    var date = DateTime.ParseExact(datum.Description, "yyyy:MM:dd HH:mm:ss", provider);
                    if (Math.Abs((fi.CreationTime - date).TotalDays) > 1)
                    {
                        Console.WriteLine($"change Date to {date:G}");
                        Directory.SetCreationTime(file, date);
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
            }
            return 0;
        }
    }
}