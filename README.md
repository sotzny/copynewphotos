# CopyNewPhotos

is a program to copy photos from a folder (with subfolders) according to criteria like time, number and size.

For a picture frame, I only need the newest pictures from the photo collection, but not too many, so as not to overload the memory.

Additionally the tool offers a command to set the EXIF creation date as file creation date. This is a prerequisite for the Copy command to work correctly.

Help is displayed when you start the program.